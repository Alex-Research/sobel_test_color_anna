//
//  ParametersConfig.h
//  Density
//
//  Created by Anna Yang on 4/24/18.
//  Copyright © 2018 imagosystems. All rights reserved.
//

#ifndef ParametersConfig_h
#define ParametersConfig_h

#include <fstream>
#include <sstream>
#include <string>
using namespace std;

class ParameterConfig {
    
public:
    ParameterConfig(string fileName){
        bool status = read(fileName);
        if (status == false) throw -1;
    };
    ~ParameterConfig(){};
    bool enable_weighted_gray = false;
    bool enable_hsi = false;
    int hue_method = 1;
    int choose_channel = 1;
    int sobel_mask_size = 3;
    int sobel_mask_version = 1;
    bool use_sum_gradients = true;
    bool using_only_black_and_white_colors = false;
    bool black_background_white_contours = false;
    
    float fFadingFactor;
    float fWeight_Red;
    float fWeight_Green;
    float fWeight_Blue;
    float fWeight_For_5x5_Sobel_Filter;
    float fNormalizing;
    float fWeight_For_3x3_2nd_Version;
    
private:
    bool read(string fileName){
        ifstream fin(fileName);
        string line;
        while (getline(fin, line))
        {
            istringstream in(line);
            string name;
            in >> name;
            if (name == "enable_weighted_gray"){
                in >> enable_weighted_gray;
            }
            if (name == "enable_hsi"){
                in >> enable_hsi;
            }
            if (name == "hue_method"){
                in >> hue_method;
            }
            if (name == "choose_channel"){
                in >> choose_channel;
            }
            if (name == "fFadingFactor"){
                in >> fFadingFactor;
            }
            if (name == "fWeight_Red"){
                in >> fWeight_Red;
            }
            if (name == "fWeight_Green"){
                in >> fWeight_Green;
            }
            if (name == "fWeight_Blue"){
                in >> fWeight_Blue;
            }
            if (name == "sobel_mask_size"){
                in >> sobel_mask_size;
            }
            if (name == "sobel_mask_version"){
                in >> sobel_mask_version;
            }
            if (name == "use_sum_gradients"){
                in >> use_sum_gradients;
            }
            if (name == "using_only_black_and_white_colors"){
                in >> using_only_black_and_white_colors;
            }
            if (name == "black_background_white_contours"){
                in >> black_background_white_contours;
            }
            if (name == "fWeight_For_5x5_Sobel_Filter"){
                in >> fWeight_For_5x5_Sobel_Filter;
            }
            if (name == "fWeight_For_3x3_2nd_Version"){
                in >> fWeight_For_3x3_2nd_Version;
            }
            if (name == "fNormalizing"){
                in >> fNormalizing;
            }
        }
        return validate();
    };
    
    bool validate()
    {
        float fSumOfPercentsf = fWeight_Red + fWeight_Green + fWeight_Blue;
        float fDeviationOfSumFrom_100percentsf = 100.0 - fSumOfPercentsf;
        
        if (fDeviationOfSumFrom_100percentsf < 0.0)
            fDeviationOfSumFrom_100percentsf = -fDeviationOfSumFrom_100percentsf;
        
        if (fDeviationOfSumFrom_100percentsf > 1.0)
        {
            printf("\n\n An error: the sum of the color weights deviates too much from 100 percents");
            printf("\n\n Please adjust the color weights");
            return false;
        }

        if (!enable_weighted_gray && !enable_hsi){
            
            printf("\n\n An error: either Weighted_Gray or HSI must be chosen to generate the edges");
            return false;
        }
        else if (enable_hsi){
            if (enable_weighted_gray){
                printf("\n\n An error: either Weighted_Gray or HSI could be applied to generate the edges; they could not be used both at the same time");
                return false;
            }
            if (choose_channel < 1 || choose_channel > 3){
                printf("\n\n An error 1: only one of Hue, Saturation or Intensity could be used to generate the edges ");
                return false;
            }
        }
        
        return true;
    };
};
#endif /* ParametersConfig_h */

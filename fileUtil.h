//
//  fileUtil.h
//  Density
//
//  Created by Anna Yang on 4/24/18.
//  Copyright © 2018 imagosystems. All rights reserved.
//

#ifndef fileUtil_h
#define fileUtil_h

#include "glob.h"
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>
#include <vector>
using namespace std;

vector<string> getFilePaths(const string& pattern){
    glob_t glob_result;
    glob(pattern.c_str(),GLOB_TILDE,NULL,&glob_result);
    vector<string> files;
    for(unsigned int i=0;i<glob_result.gl_pathc;++i){
        files.push_back(string(glob_result.gl_pathv[i]));
    }
    globfree(&glob_result);
    return files;
}

void listdir(string name, vector<string>& inDirs){
    DIR *dir;
    struct dirent *entry;
    
    if (!(dir = opendir(name.c_str())))
        throw "Cannot open txt file!";
    
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_DIR) {
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;
            string path = name + "/" + string(entry->d_name);
            inDirs.push_back(path);
            //string pathToCreate = base + "/" + string(entry->d_name);
            //outDirs.push_back(pathToCreate);
            //mkdir(pathToCreate.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
            
            listdir(path, inDirs);
        }
    }
    closedir(dir);
}

bool isFileExist(string file){
    return (access(file.c_str(), F_OK) != -1);
}

#endif /* fileUtil_h */
